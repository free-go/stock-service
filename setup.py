from distutils.core import setup

setup(
    name="stock-service",
    version="1.0",
    description="Stock micro service",
    author="Jérémy Merle",
    author_email="jeremy.merle@protonmail.com",
)
