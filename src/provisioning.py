from app import create_app
from app.models import Stock, StockUser, db


def provisioning(db):
    s1 = Stock("Frigo maison", 1)
    su1 = StockUser(1, 1)

    db.session.add(s1)
    db.session.add(su1)
    db.session.commit()


if __name__ == "__main__":
    try:
        app = create_app()
        db.app = app
        db.drop_all()
        print("Database successfuly dropped !")
        db.create_all()
        print("Database successfuly created !")
        provisioning(db)
    except Exception as e:
        print(e)
