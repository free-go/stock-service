import json
import unittest

from app import create_app
from app.models import db
from provisioning import provisioning


class BasicTest(unittest.TestCase):
    def setUp(self):
        app = create_app()
        app.config["TESTING"] = True
        app.config["WTF_CSRF_ENABLED"] = False
        app.config["DEBUG"] = False
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///freego.sqlite3"
        self.app = app

        db.init_app(self.app)
        with self.app.app_context():
            db.drop_all()
            db.create_all()
            provisioning(db)
        self.app = app.test_client()

    def tearDown(self):
        pass

    def test_get_all_stocks(self):
        rv = self.app.get("/stock", environ_base={"HTTP_X_AUTH_USER": 1})
        assert len(rv.json) == 1
        assert rv.status_code == 200
        assert rv.json[0] == {"id": 1, "name": "Frigo maison", "owner": 1}

    def test_get_a_stock_success(self):
        rv = self.app.get("/stock/1", environ_base={"HTTP_X_AUTH_USER": 1})
        assert rv.status_code == 200
        assert rv.json == {"id": 1, "name": "Frigo maison", "owner": 1}

    def test_get_a_stock_fail(self):
        rv = self.app.get("/stock/2", environ_base={"HTTP_X_AUTH_USER": 2})
        assert rv.status_code == 404

    def test_create_a_stock_success(self):
        data = json.dumps({"name": "Test de POST"})
        rv = self.app.post(
            "/stock",
            data=data,
            content_type="application/json",
            environ_base={"HTTP_X_AUTH_USER": 2},
        )
        assert rv.status_code == 201
        rv = self.app.get("/stock/2", environ_base={"HTTP_X_AUTH_USER": 2})
        assert rv.status_code == 200
        assert rv.json == {"id": 2, "name": "Test de POST", "owner": 2}

    def test_create_a_stock_fail(self):
        data = json.dumps({"azerty": "Test de POST"})
        rv = self.app.post(
            "/stock",
            data=data,
            content_type="application/json",
            environ_base={"HTTP_X_AUTH_USER": 1},
        )
        assert rv.status_code == 400
        # No stock created
        rv = self.app.get("/stock", environ_base={"HTTP_X_AUTH_USER": 1})
        assert len(rv.json) == 1
        assert rv.status_code == 200

    def test_update_a_stock_success(self):
        data = json.dumps({"name": "Nouveau nom"})
        rv = self.app.put(
            "/stock/1",
            data=data,
            content_type="application/json",
            environ_base={"HTTP_X_AUTH_USER": 1},
        )
        assert rv.status_code == 200
        assert rv.json == {"id": 1, "name": "Nouveau nom", "owner": 1}
        # The stock has been changed
        rv = self.app.get("/stock/1", environ_base={"HTTP_X_AUTH_USER": 1})
        assert rv.status_code == 200
        assert rv.json == {"id": 1, "name": "Nouveau nom", "owner": 1}

    def test_update_a_stock_forbiden(self):
        data = json.dumps({"name": "Nouveau nom"})
        rv = self.app.put(
            "/stock/1",
            data=data,
            content_type="application/json",
            environ_base={"HTTP_X_AUTH_USER": 2},
        )
        assert rv.status_code == 403

    def test_update_a_stock_invalid_json(self):
        data = json.dumps({"azerty": "Nouveau nom"})
        rv = self.app.put(
            "/stock/1",
            data=data,
            content_type="application/json",
            environ_base={"HTTP_X_AUTH_USER": 1},
        )
        # Invalid json should cause error
        assert rv.status_code == 400
        # The stock has not been changed
        rv = self.app.get("/stock/1", environ_base={"HTTP_X_AUTH_USER": 1})
        assert rv.status_code == 200
        assert rv.json == {"id": 1, "name": "Frigo maison", "owner": 1}

    def test_delete_a_stock(self):
        rv = self.app.delete("/stock/1", environ_base={"HTTP_X_AUTH_USER": 1})
        assert rv.status_code == 200
        assert rv.json == {"id": 1}
        rv = self.app.get("/stock/1", environ_base={"HTTP_X_AUTH_USER": 1})
        assert rv.status_code == 404

    def test_delete_not_owner(self):
        rv = self.app.delete("/stock/1", environ_base={"HTTP_X_AUTH_USER": 2})
        assert rv.status_code == 403
        # Th stock still exists
        rv = self.app.get("/stock/1", environ_base={"HTTP_X_AUTH_USER": 1})
        assert rv.status_code == 200

    def test_add_user(self):
        data = json.dumps({"user_id": 2})
        rv = self.app.post(
            "/stock/1/adduser",
            data=data,
            content_type="application/json",
            environ_base={"HTTP_X_AUTH_USER": 1},
        )
        assert rv.status_code == 200
        rv = self.app.get("/stock", environ_base={"HTTP_X_AUTH_USER": 2})
        assert len(rv.json) == 1
        assert rv.status_code == 200

    def test_add_user_invalid(self):
        data = json.dumps({"yolo": 2})
        rv = self.app.post(
            "/stock/1/adduser",
            data=data,
            content_type="application/json",
            environ_base={"HTTP_X_AUTH_USER": 2},
        )
        assert rv.status_code == 422

    def test_add_user_forbiden(self):
        data = json.dumps({"user_id": 2})
        rv = self.app.post(
            "/stock/1/adduser",
            data=data,
            content_type="application/json",
            environ_base={"HTTP_X_AUTH_USER": 2},
        )
        assert rv.status_code == 403

    def test_add_user_already_added(self):
        data = json.dumps({"user_id": 1})
        rv = self.app.post(
            "/stock/1/adduser",
            data=data,
            content_type="application/json",
            environ_base={"HTTP_X_AUTH_USER": 1},
        )
        assert rv.status_code == 403
