import requests
from flask import Blueprint, abort, jsonify, request

from app.models import Stock, StockUser, db
from app.utils import model_to_json

api_controller = Blueprint("api_controller", __name__)


@api_controller.route("/stock")
def get_stocks():
    user_id = request.headers["X-Auth-User"]
    res = StockUser.query.filter_by(user_id=user_id).all()
    return model_to_json(res)


@api_controller.route("/stock/<int:stock_id>")
def get_stock(stock_id):
    user_id = request.headers["X-Auth-User"]
    res = StockUser.query.filter_by(stock_id=stock_id, user_id=user_id).first_or_404()
    return model_to_json(res)


@api_controller.route("/stock", methods=["POST"])
def create_stock():
    if not request.json or "name" not in request.json:
        abort(400)
    user_id = request.headers["X-Auth-User"]
    stock = Stock(request.json.get("name"), user_id)
    db.session.add(stock)
    db.session.commit()
    su = StockUser(stock._id, user_id)
    db.session.add(su)
    db.session.commit()
    return model_to_json(stock), 201


@api_controller.route("/stock/<int:stock_id>", methods=["DELETE"])
def delete_stock(stock_id):
    # Must check if the user is the owner before deleting
    stock = Stock.query.get(stock_id)
    user_id = request.headers["X-Auth-User"]
    if stock.owner != int(user_id):
        abort(403)
    for su in StockUser.query.filter_by(stock_id=stock_id).all():
        db.session.delete(su)
    db.session.delete(Stock.query.get(stock_id))
    db.session.commit()
    return jsonify({"id": stock_id})


@api_controller.route("/stock/<int:stock_id>", methods=["PUT"])
def update_stock(stock_id):
    user_id = request.headers["X-Auth-User"]
    stock = Stock.query.get(stock_id)
    if stock.owner != int(user_id):
        abort(403)
    if not request.json or "name" not in request.json:
        abort(400)
    stock.name = request.json.get("name", stock.name)
    db.session.commit()
    return model_to_json(stock)


@api_controller.route("/stock/<int:stock_id>/adduser", methods=["POST"])
def adduser_stock(stock_id):
    current_user_id = int(request.headers["X-Auth-User"])

    if not request.is_json:
        abort(400)

    user_id = request.json.get("user_id")
    if not user_id:
        abort(422)

    # The user that made the request should have access to the stock
    stock = Stock.query.get(stock_id)
    if stock.owner != current_user_id:
        abort(403)

    # The user to be added shouldn't already have access to the stock
    user_request = StockUser.query.filter_by(
        stock_id=stock_id, user_id=user_id
    ).first()
    if user_request:
        abort(403)

    stock_user = StockUser(stock_id, user_id)
    db.session.add(stock_user)
    db.session.commit()

    return model_to_json(stock)


@api_controller.route("/stock/<int:stock_id>/users")
def listusers_stock(stock_id):
    stock = Stock.query.get(stock_id)
    current_user_id = int(request.headers["X-Auth-User"])

    # Only the owner can view the users
    if current_user_id != stock.owner:
        abort(403)

    stock_users = []
    for stock_user in StockUser.query.filter_by(stock_id=stock_id).all():
        # Don't include the owner in the list
        if stock_user.user_id == stock.owner:
            continue

        r = requests.get(f"http://auth-service:8080/auth/user/{stock_user.user_id}")
        if r.status_code == 200:
            stock_users.append(r.json())

    return jsonify(stock_users)


@api_controller.route("/stock/<int:stock_id>/user/<int:user_id>", methods=["DELETE"])
def deleteuser_stock(stock_id, user_id):
    stock = Stock.query.get(stock_id)
    current_user_id = int(request.headers["X-Auth-User"])

    # Only the owner can delete and can't delete himself from a stock he owns
    if stock.owner != current_user_id or stock.owner == user_id:
        abort(403)

    stock_user = StockUser.query.filter_by(stock_id=stock_id, user_id=user_id).first()

    if stock_user:
        db.session.delete(stock_user)
        db.session.commit()
    else:
        abort(404)

    return jsonify({"stock_id": stock_id, "user_id": user_id})
