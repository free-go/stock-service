from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

db = SQLAlchemy()


class StockUser(db.Model):
    __tablename__ = "stock_user"
    _id = db.Column("id", db.Integer, primary_key=True)
    stock_id = db.Column(db.Integer, ForeignKey("stock.id"))
    stock = relationship("Stock")
    user_id = db.Column("user_id", db.Integer)

    def __init__(self, stock_id, user_id):
        self.stock_id = stock_id
        self.user_id = user_id

    def serialize(self):
        return self.stock.serialize()


class Stock(db.Model):
    _id = db.Column("id", db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    owner = db.Column(db.Integer, nullable=False)

    def __init__(self, name, owner):
        self.name = name
        self.owner = owner

    def serialize(self):
        return {"id": self._id, "name": self.name, "owner": self.owner}
