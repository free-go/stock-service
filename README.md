# stock-service

Micro-service running everything stock related.

## Docker

To build the docker & run it manually :

```bash
# In the project root folder
docker build -t stock-service .

# Port 5000 is Flask standard
docker run -d --name stock-service -p 5000:8080 stock-service
```

Type|URL|Action| Request content | Response
---|---|---|---|---
`GET`|`/stock`| List of all stocks | - | `[{id: 1, name: "F1"}]`
`POST`|`/stock`| Create a stock | `{name: 'F1'}` | `{id: 1, name: "F1"}`
`PUT`|`/stock/<int:stock_id>`| Update a stock stock | `{name: 'F1'}` | `{id: 1, name: "F1"}`
`DELETE`|`/stock/<int:stock_id>`| Delete a stock | - | `{id: 1}`
`GET`|`/stock/<int:stock_id>`| Information about a precise stock | - | `{id: 1, name: "F1"}`
`GET`|`/stock/<int:stock_id>/users`| Users for a stock (without its owner) | - | `[{id: 1, name: "test", email: "test@test.test"}]`
`POST`|`/stock/<int:stock_id>/adduser`| Add user to a stock | `{user_id: 2}` | `{"id": 1, "FreeGo1": self.name, "owner": 1}`
`DELETE`|`/stock/<int:stock_id>/user/<int:user_id>`| Delete user from a stock | - | `{"stock_id": 1, "user_id": 2}`
